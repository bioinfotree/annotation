### phase_2.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:



TEST ?=
PRJ ?=
INPUT_FASTA ?=
DB-PATH_ACIPENSER_EST ?=
DB-NAME_ACIPENSER_EST ?=
DB-PATH_EL-SHARK_EST ?=
DB-NAME_EL-SHARK_EST ?=
CPUS ?=
SPECIE ?=
EVAL ?=


# links
query.fasta: $(INPUT_FASTA)
	ln -sf $< $@


# ACIPENSER EST
acipenser_EST.flag: $(DB-PATH_ACIPENSER_EST)
	$(call link_DB, $</$(DB-NAME_ACIPENSER_EST)*, $@)


acipenser_EST.xml.gz: query.fasta acipenser_EST.flag
	!threads
	tblastx -evalue 0.001 -num_threads $$THREADNUM -outfmt 5 -max_target_seqs 20 -query $< -db ./$(basename $^2)/$(basename $^2) -out $@; \
	if gzip -cv $(basename $@) > $@; \
	then \
	rm $(basename $@); \
	fi




# ELEPHANT_SHARK cDNA
elephant-shark_EST.flag: $(DB-PATH_EL-SHARK_EST)
	$(call link_DB, $</$(DB-NAME_EL-SHARK_EST)*, $@)


elephant-shark_EST.xml.gz: query.fasta elephant-shark_EST.flag
	!threads
	$(call search_DB, $<, ./$(basename $^2)/$(basename $^2), $@)







######################################################################################
SPECIE_XML_GZ = $(addsuffix _EST.xml.gz, $(SPECIE))
%_EST.xml.gz: query.fasta %_EST.flag
	!threads
	$(call search_DB, $<, ./$(basename $^2)/$(basename $^2), $@)

SPECIE_TAB = $(addsuffix _EST.tab, $(SPECIE))
%_EST.tab: %_EST.xml.gz
	$(call parse_result, $<, $@)

SPECIE_NO_HL = $(addsuffix _EST.no-hit.lst, $(SPECIE))
# list of sequences whithout hit
%_EST.no-hit.lst: %_EST.tab query.fasta
	comm -2 -3 --check-order <(bsort <(fasta2tab < $^2 | cut -f 1)) \
	<(bsort <(cut -f 6 < $<)) > $@

SPECIE_HL = $(addsuffix _EST.hit.lst, $(SPECIE))
%_EST.hit.lst: %_EST.tab
	cut -f 6 < $< > $@

SPECIE_GENE = $(addsuffix _EST.gene.lst, $(SPECIE))
%_EST.gene.lst: %_EST.tab
	cut -f 9 $< | cut -d " " -f 4 | bsort | uniq -c > $@

SPECIE_TOT_GENE = $(addsuffix _EST.tot_gene.lst, $(SPECIE))
%_EST.tot_gene.lst: %_EST.flag
	fasta2tab < $(basename $<)/$(basename $<) | cut -d " " -f 4 | bsort | uniq -c > $@

SPECIE_SBJ = $(addsuffix _EST.subject.lst, $(SPECIE))
%_EST.subject.lst: %_EST.tab
	cut -f 9 $< | cut -d " " -f 1 | bsort | uniq -c > $@





define link_DB
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $2) && \
	cd $(basename $2) && \
	_comment "cDNA sequence downloaded from genome project" && \
	for FILE in $1 ; do \
	ln -sf $$FILE ; \
	done && \
	cd .. && \
	touch $2
endef



define download_DB
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $3) && \
	cd $(basename $3) && \
	date | bawk '!/^[$$,\#+]/ { \
	{ print "database_downloaded:",$$0; } \
	}' > DB.info && \
	_comment "cDNA sequence downloaded from ensembl release 66" && \
	wget -q -O $(basename $3).gz $1 && \
	wget -q  $2 && \
	gunzip $(basename $3).gz && \
	makeblastdb -in $(basename $3) -dbtype nucl -title $(basename $3) && \
	cd .. && \
	touch $3
endef


define search_DB
	tblastx -evalue 0.001 -num_threads $$THREADNUM -outfmt 5 -max_target_seqs 20 -query $1 -db $2 -out $(basename $3); \
	if gzip -cv $(basename $3) > $3; \
	then \
	rm $(basename $3); \
	fi
endef


define parse_result
	zcat -dc $1 | blast_fields -f -r application,database_length,database,database_sequences,expect,query,query_length -a accession,hit_def,hit_id,length -p align_length,bits,expect,frame,gaps,identities,num_alignments,positives,query,query_end,query_start,sbjct,sbjct_end,sbjct_start,score,strand -e $(EVAL) > $2
endef

#######################################################################################





SPECIE_DB = $(addsuffix _EST, $(SPECIE))
SPECIE_FLAG = $(addsuffix _EST.flag, $(SPECIE))

# LAMPREY cDNA
LAMPREY_EST = ftp://ftp.ensembl.org/pub/release-66/fasta/petromyzon_marinus/cdna/Petromyzon_marinus.Pmarinus_7.0.66.cdna.all.fa.gz
README = ftp://ftp.ensembl.org/pub/release-66/fasta/petromyzon_marinus/cdna/README

lamprey_EST.flag:
	$(call download_DB, $(LAMPREY_EST), $(README), $@)




# DANIO cDNA
DANIO_EST = ftp://ftp.ensembl.org/pub/release-66/fasta/danio_rerio/cdna/Danio_rerio.Zv9.66.cdna.all.fa.gz

danio_EST.flag:
	$(call download_DB, $(DANIO_EST), $(README), $@)


# # NACCARII cDNA
# naccarii_EST.flag: query.fasta
# 	_comment () { echo -n ""; }; \
# 	mkdir -p $(basename $@) && \
# 	cd $(basename $@) && \
# 	date | bawk '!/^[$$,\#+]/ { \
# 	{ print "database_downloaded:",$$0; } \
# 	}' > DB.info && \
# 	ln -sf ../$< $(basename $@) && \
# 	_comment "cDNA sequence downloaded from ensembl release 66" && \
# 	makeblastdb -in $(basename $@) -dbtype nucl -title $(basename $@) && \
# 	cd .. && \
# 	touch $@

# adar.xml: $(ADAR) naccarii_EST.flag
# 	tblastx -evalue 0.001 -num_threads $(CPUS) -outfmt 5 -max_target_seqs 20 -query $< -db ./$(basename $^2)/$(basename $^2) -out $@


# adar.tab: adar.xml
# 	blast_fields -r application,database_length,database,database_sequences,expect,query,query_length -a accession,hit_def,hit_id,length -p align_length,bits,expect,frame,gaps,identities,num_alignments,positives,query,query_end,query_start,sbjct,sbjct_end,sbjct_start,score,strand $< > $@




# FUGU cDNA
FUGU_EST =  ftp://ftp.ensembl.org/pub/release-66/fasta/takifugu_rubripes/cdna/Takifugu_rubripes.FUGU4.66.cdna.all.fa.gz

fugu_EST.flag:
	$(call download_DB, $(FUGU_EST), $(README), $@)


# TETRAODON cDNA
TETRAODON_EST = ftp://ftp.ensembl.org/pub/release-66/fasta/tetraodon_nigroviridis/cdna/Tetraodon_nigroviridis.TETRAODON8.66.cdna.all.fa.gz

tetraodon_EST.flag:
	$(call download_DB, $(TETRAODON_EST), $(README), $@)


# STICKLEBACK cDNA
STICKLEBACK_EST = ftp://ftp.ensembl.org/pub/release-66/fasta/gasterosteus_aculeatus/cdna/Gasterosteus_aculeatus.BROADS1.66.cdna.all.fa.gz

stickleback_EST.flag:
	$(call download_DB, $(STICKLEBACK_EST), $(README), $@)



# MEDAKA cDNA
MEDAKA_EST = ftp://ftp.ensembl.org/pub/release-66/fasta/oryzias_latipes/cdna/Oryzias_latipes.MEDAKA1.66.cdna.all.fa.gz

medaka_EST.flag:
	$(call download_DB, $(MEDAKA_EST), $(README), $@)



# COELACANTH cDNA
COELACANTH_EST = ftp://ftp.ensembl.org/pub/release-66/fasta/latimeria_chalumnae/cdna/Latimeria_chalumnae.LatCha1.66.cdna.all.fa.gz

coelacanth_EST.flag:
	$(call download_DB, $(COELACANTH_EST), $(README), $@)


# COD cDNA
COD_EST = ftp://ftp.ensembl.org/pub/release-66/fasta/gadus_morhua/cdna/Gadus_morhua.gadMor1.66.cdna.all.fa.gz

cod_EST.flag:
	$(call download_DB, $(COD_EST), $(README), $@)



# COD cDNA
HSAPIENS_EST = ftp://ftp.ensembl.org/pub/release-66/fasta/homo_sapiens/cdna/Homo_sapiens.GRCh37.66.cdna.all.fa.gz

hsapiens_EST.flag:
	$(call download_DB, $(HSAPIENS_EST), $(README), $@)








.PHONY: test
test:
	@echo $(SPECIE_XML_GZ)


# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += query.fasta \
	 $(SPECIE_XML_GZ) \
	 $(SPECIE_HL) \
	 $(SPECIE_NO_HL) \
	 $(SPECIE_SBJ) \
	 $(SPECIE_GENE) \
	 $(SPECIE_TOT_GENE)





# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += query.fasta \
	 $(SPECIE_XML_GZ) \
	 $(SPECIE_TAB) \
	 $(SPECIE_NO_HL) \
	 $(SPECIE_HL) \
	 $(SPECIE_GENE) \
	 $(SPECIE_SBJ) \
	 $(SPECIE_TOT_GENE)
	 #$(SPECIE_DB)
	 #$(SPECIE_FLAG) 




######################################################################
### phase_2.mk ends here
