BLAST_RESULTS ?=


split.flag: $(BLAST_RESULTS)
	mkdir -p $(basename $@); \
	cd $(basename $@); \
	split_xml_blast ../$<; \
	cd ..; \
	touch $@




# # recompose long XML file from fragments
# nr.xml: split.flag
# 	CHUNCKS=`ls -1 $(basename $<)/*.xml \
# 	| bawk '!/^[$$,\#+]/ { \
# 	{ printf "%s " , $$0; } \
# 	}'` \
# 	cd $(basename $<) \
# 	for FILE in $$(ls); do echo $$FILE; done
# #	mergexml -o $@ -i $$CHUNCKS



# nr.xml: split.flag
# 	i=0; \
# 	j=200; \
# 	CHUNCKS=""; \
# 	cd $(basename $<); \
# 	for FILE in $$( ls ) ; do \
# 	let i=i+1; \
# 	CHUNCKS="$$CHUNCKS $$FILE" ; \
# 	if [ $$i -eq "200" ]; then \
# 	echo -e "$$CHUNCKS\n$$j"_$@"\n\n" >> ../log/done.log; \
# 	mergexml -o ../done/"$$j"_$@ -i $$CHUNCKS; \
# 	CHUNCKS=""; \
# 	i=0; \
# 	let j=j+200; \
# 	fi \
# 	done; \
# 	mergexml -o ../done/final_$@ -i $$CHUNCKS; \
# 	echo -e "$$CHUNCKS\n$$j"_$@"\n\n" >> ../log/done.log; \
# 	cd ..; \
# 	mv done done_1; \
# 	touch done_1.flag


# nr.xml: done_1.flag
# 	mkdir -p done; \
# 	i=0; \
# 	j=200; \
# 	CHUNCKS=""; \
# 	cd $(basename $<); \
# 	for FILE in $$( ls ) ; do \
# 	let i=i+1; \
# 	CHUNCKS="$$CHUNCKS $$FILE" ; \
# 	if [ $$i -eq "200" ]; then \
# 	echo -e "$$CHUNCKS\n$$j"_$@"\n\n" >> ../log/$(basename $<).log; \
# 	mergexml -o ../done/"$$j"_$@ -i $$CHUNCKS; \
# 	CHUNCKS=""; \
# 	i=0; \
# 	let j=j+200; \
# 	fi \
# 	done; \
# 	mergexml -o ../done/final_$@ -i $$CHUNCKS; \
# 	echo -e "$$CHUNCKS\n$$j"_$@"\n\n" >> ../log/$(basename $<).log; \
# 	cd ..; \
# 	mv done done_2; \
# 	touch done_2.flag



nr.xml: done_2.flag
	mkdir -p done; \
	i=0; \
	j=200; \
	CHUNCKS=""; \
	cd $(basename $<); \
	for FILE in $$( ls ) ; do \
	let i=i+1; \
	CHUNCKS="$$CHUNCKS $$FILE" ; \
	if [ $$i -eq "200" ]; then \
	echo -e "$$CHUNCKS\n$$j"_$@"\n\n" >> ../log/$(basename $<).log; \
	mergexml -o ../done/"$$j"_$@ -i $$CHUNCKS; \
	CHUNCKS=""; \
	i=0; \
	let j=j+200; \
	fi \
	done; \
	mergexml -o ../done/final_$@ -i $$CHUNCKS; \
	echo -e "$$CHUNCKS\n$$j"_$@"\n\n" >> ../log/$(basename $<).log; \
	cd ..; \
	mv done done_3; \
	touch done_3.flag


#############################################################

# test blastx with BLAST archive output format ASN (a structured format similar to XML)
# it contains:
#	The query sequence, 
	# the BLAST options,
	# the masking information, 
	# the name of the database, 	
	# the alignment

# links
query.fasta: $(INPUT_FASTA)
	ln -sf $< $@


# DROSOPHILA PROTEINS
DROSOPHILA_PROT = ftp://ftp.ensemblgenomes.org/pub/metazoa/release-14/fasta/drosophila_melanogaster/pep/Drosophila_melanogaster.BDGP5.14.pep.all.fa.gz
README = ftp://ftp.ensemblgenomes.org/pub/metazoa/release-14/fasta/drosophila_melanogaster/pep/README

# download db
drosophila_PROT.flag:
	$(call download_DB, $(DROSOPHILA_PROT), $(README), $@)

define download_DB
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $3) && \
	cd $(basename $3) && \
	date | bawk '!/^[$$,\#+]/ { \
	{ print "database_downloaded:",$$0; } \
	}' > DB.info && \
	_comment "cDNA sequence downloaded from ensembl release 66" && \
	wget -q -O $(basename $3).gz $1 && \
	wget -q  $2 && \
	gunzip $(basename $3).gz && \
	makeblastdb -in $(basename $3) -dbtype prot -title $(basename $3) && \
	cd .. && \
	touch $3
endef




# blastx search using ASN as output format
%_PROT.asn: query.fasta %_PROT.flag
	$(call search_DB, $<, ./$(basename $^2)/$(basename $^2), $@)

define search_DB
	blastx -evalue 0.001 -num_threads $(CPUS) -outfmt 11 -max_target_seqs 20 -query $1 -db $2 -out $3
endef


# convert blast archive format into xml format
%_PROT.xml: %_PROT.asn
	blast_formatter -archive $< -outfmt 5 -out $@

# convert from ASN to tabular with comment lines using custom format specifications
%_PROT.tab: %_PROT.asn
	blast_formatter -archive $< -outfmt "7 qacc sacc evalue qstart qend sstart send" -out $@

# convert from ASN to tabular using custom format
%_PROT.tab1: %_PROT.asn
	blast_formatter -archive $< -outfmt "6 qacc qlen sacc slen evalue qstart qend sstart send length" -out $@





# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += query.fasta \
	 nr.all.tab \
	 nr.tab \
	 nr.no-hit.lst \
	 nr.no-hit.fasta \
	 swissprot.flag \
	 swissprot.tab \
	 nr.no-hit.qual \
	 nr.hit.fasta \
	 nr.hit.qual




# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:
	$(RM) -rf $(TMP_DIR)
	$(RM) -rf swissprot
