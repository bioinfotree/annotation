### phase_2.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:



TEST ?=
PRJ ?=
INPUT_FASTA ?=
BLAST_DB_PATH ?=
BLAST_DB_NAME ?=
CPUS ?=


# links
query.fasta: $(INPUT_FASTA)
	ln -sf $< $@


blast_results.xml: query.fasta $(BLAST_DB_PATH)
	blastx -evalue 0.001 -num_threads 4 -outfmt 5 -max_target_seqs 20 -query $< -db $^2/$(BLAST_DB_NAME) -out $@


blast_results.tab: blast_results.xml
	blast_fields -r application,database_length,database,database_sequences,expect,query,query_length -a accession,hit_def,hit_id,length -p align_length,bits,expect,frame,gaps,identities,num_alignments,positives,query,query_end,query_start,sbjct,sbjct_end,sbjct_start,score $< > $@




blast_results_mess.xml: blast_results.xml
	mess_blast_results $< > $@


blast_results_mess.tab: blast_results_mess.xml
	blast_fields -r application,database_length,database,database_sequences,expect,query,query_length -a accession,hit_def,hit_id,length -p align_length,bits,expect,frame,gaps,identities,num_alignments,positives,query,query_end,query_start,sbjct,sbjct_end,sbjct_start,score $< > $@

blast_results_diff.txt: blast_results.tab blast_results_mess.tab
	diff <(bsort -n -k 8,8 $<) <(bsort -n -k 8,8 $^2) > $@


.PHONY: test
test:
	@echo $(454_INPUT)


# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += query.fasta \
	 blast_results.xml

# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += query.fasta \
	 blast_results.xml


# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:
	$(RM) -rf $(TMP_DIR)

######################################################################
### phase_2.mk ends here
