### phase_4.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:



TEST ?=
PRJ ?=
INPUT_FASTA ?=
CPUS ?=


THIS_SPECIE = $(call set_remove,acipenser,$(call set_remove,elephant-shark,$(call set_create, $(SPECIE))))


# links
query.fasta: $(INPUT_FASTA)
	ln -sf $< $@


######################################################################################

SPECIE_XML_GZ = $(addsuffix _NC.xml.gz, $(THIS_SPECIE))
%_NC.xml.gz: query.fasta %_NC.flag
	!threads
	$(call search_DB, $<, ./$(basename $^2)/$(basename $^2), $@)

SPECIE_TAB = $(addsuffix _NC.tab, $(THIS_SPECIE))
%_NC.tab: %_NC.xml.gz
	$(call parse_result, $<, $@)


SPECIE_NO_HL = $(addsuffix _NC.no-hit.lst, $(THIS_SPECIE))
# list of sequences whithout hit
%_NC.no-hit.lst: %_NC.tab query.fasta
	comm -2 -3 --check-order <(bsort <(fasta2tab < $^2 | cut -f 1)) \
	<(bsort <(cut -f 6 < $<)) > $@

SPECIE_HL = $(addsuffix _NC.hit.lst, $(THIS_SPECIE))
%_NC.hit.lst: %_NC.tab
	cut -f 6 < $< > $@

SPECIE_SBJ = $(addsuffix _NC.subject.lst, $(THIS_SPECIE))
%_NC.subject.lst: %_NC.tab
	cut -f 9 $< | cut -d " " -f 1,2 | bsort | uniq -c > $@

SPECIE_GENE = $(addsuffix _NC.gene.lst, $(THIS_SPECIE))
%_NC.gene.lst: %_NC.tab
	cut -f 9 $< | cut -d " " -f 4 | bsort | uniq -c > $@

SPECIE_TOT_GENE = $(addsuffix _NC.tot_gene.lst, $(THIS_SPECIE))
%_NC.tot_gene.lst: %_NC.flag
	fasta2tab < $(basename $<)/$(basename $<) | cut -d " " -f 4 | bsort | uniq -c > $@

SPECIE_SBJ_TYPE = $(addsuffix _NC.subject.info, $(THIS_SPECIE))
%_NC.subject.info: %_NC.subject.lst
	cut -d ":" -f 2 $< | bsort | uniq -c | tr -s " " \\t > $@




define download_DB
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $3) && \
	cd $(basename $3) && \
	date | bawk '!/^[$$,\#+]/ { \
	{ print "database_downloaded:",$$0; } \
	}' > DB.info && \
	_comment "ncRNA sequence downloaded from ensembl release 66" && \
	wget -q -O $(basename $3).gz $1 && \
	wget -q  $2 && \
	gunzip $(basename $3).gz && \
	makeblastdb -in $(basename $3) -dbtype nucl -title $(basename $3) && \
	cd .. && \
	touch $3
endef


define search_DB
	blastn -evalue 0.001 -num_threads $$THREADNUM -outfmt 5 -max_target_seqs 20 -query $1 -db $2 -out $(basename $3); \
	if gzip -cv $(basename $3) > $3; \
	then \
	rm $(basename $3); \
	fi

endef


define parse_result
	zcat -dc $1 | blast_fields -f -r application,database_length,database,database_sequences,expect,query,query_length -a accession,hit_def,hit_id,length -p align_length,bits,expect,frame,gaps,identities,num_alignments,positives,query,query_end,query_start,sbjct,sbjct_end,sbjct_start,score,strand -e $(EVAL)> $2
endef


#######################################################################################







# LAMPREY ncRNA
LAMPREY_NC = ftp://ftp.ensembl.org/pub/release-66/fasta/petromyzon_marinus/ncrna/Petromyzon_marinus.Pmarinus_7.0.66.ncrna.fa.gz
README = ftp://ftp.ensembl.org/pub/release-66/fasta/petromyzon_marinus/ncrna/README

lamprey_NC.flag:
	$(call download_DB, $(LAMPREY_NC), $(README), $@)




# DANIO ncRNA
DANIO_NC = ftp://ftp.ensembl.org/pub/release-66/fasta/danio_rerio/ncrna/Danio_rerio.Zv9.66.ncrna.fa.gz

danio_NC.flag:
	$(call download_DB, $(DANIO_NC), $(README), $@)




# FUGU ncRNA
FUGU_NC = ftp://ftp.ensembl.org/pub/release-66/fasta/takifugu_rubripes/ncrna/Takifugu_rubripes.FUGU4.66.ncrna.fa.gz

fugu_NC.flag:
	$(call download_DB, $(FUGU_NC), $(README), $@)


# TETRAODON ncRNA
TETRAODON_NC = ftp://ftp.ensembl.org/pub/release-66/fasta/tetraodon_nigroviridis/ncrna/Tetraodon_nigroviridis.TETRAODON8.66.ncrna.fa.gz

tetraodon_NC.flag:
	$(call download_DB, $(TETRAODON_NC), $(README), $@)


# STICKLEBACK ncRNA
STICKLEBACK_NC = ftp://ftp.ensembl.org/pub/release-66/fasta/gasterosteus_aculeatus/ncrna/Gasterosteus_aculeatus.BROADS1.66.ncrna.fa.gz

stickleback_NC.flag:
	$(call download_DB, $(STICKLEBACK_NC), $(README), $@)



# MEDAKA ncRNA
MEDAKA_NC = ftp://ftp.ensembl.org/pub/release-66/fasta/oryzias_latipes/ncrna/Oryzias_latipes.MEDAKA1.66.ncrna.fa.gz

medaka_NC.flag:
	$(call download_DB, $(MEDAKA_NC), $(README), $@)



# COELACANTH ncRNA
COELACANTH_NC = ftp://ftp.ensembl.org/pub/release-66/fasta/latimeria_chalumnae/ncrna/Latimeria_chalumnae.LatCha1.66.ncrna.fa.gz

coelacanth_NC.flag:
	$(call download_DB, $(COELACANTH_NC), $(README), $@)


# COD ncRNA
COD_NC = ftp://ftp.ensembl.org/pub/release-66/fasta/gadus_morhua/ncrna/Gadus_morhua.gadMor1.66.ncrna.fa.gz

cod_NC.flag:
	$(call download_DB, $(COD_NC), $(README), $@)



# HSAPIENS ncRNA
HSAPIENS_NC = ftp://ftp.ensembl.org/pub/release-66/fasta/homo_sapiens/ncrna/Homo_sapiens.GRCh37.66.ncrna.fa.gz

hsapiens_NC.flag:
	$(call download_DB, $(HSAPIENS_NC), $(README), $@)








.PHONY: test
test:
	@echo $(454_INPUT)


# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += query.fasta \
	 $(SPECIE_XML_GZ) \
	 $(SPECIE_TAB) \
	 $(SPECIE_HL) \
	 $(SPECIE_NO_HL) \
	 $(SPECIE_SBJ) \
	 $(SPECIE_GENE) \
	 $(SPECIE_TOT_GENE) \
	 $(SPECIE_SBJ_TYPE)



# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += query.fasta \
	 $(SPECIE_XML_GZ) \
	 $(SPECIE_TAB) \
	 $(SPECIE_NO_HL) \
	 $(SPECIE_HL) \
	 $(SPECIE_SBJ) \
	 $(SPECIE_GENE) \
	 $(SPECIE_TOT_GENE) \
	 $(SPECIE_SBJ_TYPE)
	 #$(SPECIE_DB)
	 #$(SPECIE_FLAG) 


######################################################################
### phase_4.mk ends here
