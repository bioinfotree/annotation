### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:


context prj/454_preprocessing


TEST ?=
PRJ ?=
INPUT_FASTA ?=
INPUT_QUAL ?=
DB-PATH_NR ?=
DB-NAME_NR ?=
BLAST_RESULT_NT ?=
BLAST_RESULT_NR ?=
DB-PATH_ACIPENSER_EST ?=
DB-NAME_ACIPENSER_EST ?=
DB-PATH_EL-SHARK_EST ?=
DB-NAME_EL-SHARK_EST ?=
DB-PATH_ACIPENSER_PROT ?=
DB-NAME_ACIPENSER_PROT ?=
BLAST_RESULT_NT ?=
BLAST_RESULT_NR ?=
B2GO_GAF ?=
B2GO_KEGG ?=
ADAR ?=
BLAST_RESULTS_TMP_1 ?=
CPUS ?=
PRJ_PREFIX ?=

extern ../../../../454_assembly/dataset/$(PRJ)/phase_2/$(PRJ_PREFIX)_1_singletons.lst as 1_SINGLETONS_LST
extern ../../../../454_assembly/dataset/$(PRJ)/phase_2/$(PRJ_PREFIX)_1_no_singletons.lst as 1_NO_SINGLETONS_LST


# links
query.fasta: $(INPUT_FASTA)
	ln -sf $< $@

query.qual: $(INPUT_QUAL)
	ln -sf $< $@

$(PRJ)_1_singletons.lst: $(1_SINGLETONS_LST)
	ln -sf $< $@

$(PRJ)_1_no_singletons.lst: $(1_NO_SINGLETONS_LST)
	ln -sf $< $@

nt.xml.gz:
	ln -sf $(BLAST_RESULT_NT) $@




# tmp dir
TMP_DIR = tmp

# if not given blast result from nr, in single xml file execute blast
# by splittin query in segments
blast2go.nr.xml: query.fasta $(DB-PATH_NR)
	run_parallel_blast --input_file $< --database_file $^2/$(DB-NAME_NR) --temp_directory $(TMP_DIR) --jobs $(CPUS) --result_file $@

# recompose long XML file from segments
define reassemble_xml
	CHUNCKS="" && \
	for FILE in $1/*.blastResult ; do \
	CHUNCKS="$$CHUNCKS $$FILE" ; \
	done && \
	mergexml -o $2 -i $$CHUNCKS
endef




# if xml segments exists and are in an external dir use it
ifdef BLAST_RESULTS_TMP_1
nr.xml:
	$(call reassemble_xml,$(BLAST_RESULTS_TMP_1),$@)
# else create segments de novo with run_parallel_blast
else
nr.xml: blast2go.nr.xml
	$(call reassemble_xml,$(TMP_DIR),$@)
endif





# if xml.gz already exists
ifdef BLAST_RESULT_NR
nr.xml.gz:
	ln -sf $(BLAST_RESULT_NR) $@
# if xml.gz does not exists
else
nr.xml.gz: nr.xml
	gzip -c $< > $@

endif







# convert to tab delimited file
%.all.tab: %.xml.gz
	zcat -dc $< | blast_fields -r application,database_length,database,database_sequences,expect,query,query_length -a accession,hit_def,hit_id,length -p align_length,bits,expect,frame,gaps,identities,num_alignments,positives,query,query_end,query_start,sbjct,sbjct_end,sbjct_start,score,strand -e $(EVAL) > $@

%.tab: %.xml.gz
	zcat -dc $< | blast_fields -f -r application,database_length,database,database_sequences,expect,query,query_length -a accession,hit_def,hit_id,length -p align_length,bits,expect,frame,gaps,identities,num_alignments,positives,query,query_end,query_start,sbjct,sbjct_end,sbjct_start,score,strand -e $(EVAL) > $@


# blast_results_direct.tab: $(BLAST_RESULTS_TMP_1)
# 	for FILE in $</*.blastResult ; do \
# 	blast_fields -f -r application,database_length,database,database_sequences,expect,query,query_length -a accession,hit_def,hit_id,length -p align_length,bits,expect,frame,gaps,identities,num_alignments,positives,query,query_end,query_start,sbjct,sbjct_end,sbjct_start,score,strand $$FILE >> $@ ; \
# 	done

# add ../454/src/blasttools/blast_csv_stats.py

# list of sequences whithout hit
%.no-hit.lst: %.tab query.fasta
	comm -2 -3 --check-order <(bsort <(fasta2tab < $^2 | cut -f 1)) \
	<(bsort <(cut -f 6 < $<)) > $@

%.hit.lst: %.tab
	cut -f 6 < $< > $@

%.subject.lst: %.tab
	cut -f 10 < $< | bsort | uniq -c > $@

nr.%.contig.lst: nr.%.lst $(PRJ)_1_no_singletons.lst
	comm -1 -2 --check-order <(bsort $<) <(cut -f 1 $^2 | bsort) > $@

nr.%.singletons.lst: nr.%.lst $(PRJ)_1_singletons.lst
	comm -1 -2 --check-order <(bsort $<) <(cut -f 1 $^2 | bsort) > $@


# get fasta end quals for sequences without
%.no-hit.fasta: %.no-hit.lst query.fasta
	get_fasta --from-file $< < $^2 > $@

# get fasta end quals for sequences without
%.no-hit.qual: %.no-hit.lst query.qual
	get_fasta --from-file $< < $^2 > $@


# get fasta end quals for sequences with
%.hit.fasta: %.hit.lst query.fasta
	get_fasta --from-file $< < $^2 > $@

# get fasta end quals for sequences with
%.hit.qual: %.hit.lst query.qual
	get_fasta --from-file $< < $^2 > $@



# get statistic about GC and total length for sequence with and whithout hit
# nr.%.seq.info: nr.%.fasta nr.%.qual
# 	$(call get_info,$<,$^2)

# define get_info
# paste \
# <(count_fasta -i 50 $1 ) \
# <(fastalen $1 \
# | stat_base --mean 2 \
# | bawk '!/^[$$,\#+]/ { \
# { print "mean_length",$$0; } \
# }') \
# <(fastalen $1 \
# | stat_base --stdev 2 \
# | bawk '!/^[$$,\#+]/ { \
# { print "standard_deviation_length",$$0; } \
# }') \
# <(fastalen $1 \
# | stat_base --median 2 \
# | bawk '!/^[$$,\#+]/ { \
# { print "median_length",$$0; } \
# }') \
# <(qual_mean --total < $2 \
# | bawk '!/^[$$,\#+]/ { \
# { print "mean_quality",$$0; } \
# }') > $@
# endef




# get boxplot about GC, length and qual for sequence with and whithout hit
nr.boxplot.len.pdf: nr.no-hit.fasta nr.hit.fasta
	paste <(fasta_length $< | cut -f2) \
	<(fasta_length $^2 | cut -f2) \
	| boxplot -n --no-header --output-file $@


nr.boxplot.qual.pdf: nr.hit.qual nr.no-hit.qual
	paste <(qual_mean < $< | cut -f2) \
	<(qual_mean < $^2 | cut -f2) \
	| boxplot -n --no-header --output-file $@


nr.boxplot.GC.pdf: nr.hit.fasta nr.no-hit.fasta
	_comment () { echo -n ""; }; \
	_comment "calculate CG content % for every sequence"; \
	paste <(fasta2tab < $< | bawk '{n=length($$2);gsub(/[ATX]/,"",$$2);print $$1,(100*length($$2)/n)}' | cut -f2) \
	<(fasta2tab < $^2 | bawk '{n=length($$2);gsub(/[ATX]/,"",$$2);print $$1,(100*length($$2)/n)}'| cut -f2) \
	| boxplot -n --no-header --output-file $@


# average number of hits per transcript
# average alignment lengths of best hit
# average subject coverage fraction of best hits
# average query coverage fraction of best hits
nr.hit.aln.info: nr.all.tab nr.tab
	paste -d "\n" \
	<(cut -f 10 $< | bsort | uniq -c | bawk '!/^[$$,\#+]/ { \
	split($$0,a," ");\
	print a[1]; \
	}' | stat_base --mean --median --stdev --variance --min --max --total 1 \
	| bawk 'BEGIN { print "#mean\tmedian\tstdev\tvariance\tmin\tmax\ttotal"; } \
	!/^[$$,\#+]/ { \
	{ print "subject_per_sequence", $$0; } \
	}') \
	<(cut -f 12 $^2 | \
	stat_base --mean --median --stdev --variance --min --max --total 1 \
	| bawk '!/^[$$,\#+]/ { \
	{ print "alignment_lengths", $$0; } \
	}') \
	<(cut -f 7,12 $^2 |  bawk '!/^[$$,\#+]/ { \
	{ printf "%.4f\n", (($$2*3)/$$1*100); } \
	}' \
	| stat_base --mean --median --stdev --variance --min --max \
	| bawk '!/^[$$,\#+]/ { \
	{ print "query_coverage", $$0; } \
	}') \
	<(cut -f 11,12 $^2 |  bawk '!/^[$$,\#+]/ { \
	{ printf "%.4f\n", ($$2/$$1*100); } \
	}' \
	| stat_base --mean --median --stdev --variance --min --max \
	| bawk '!/^[$$,\#+]/ { \
	{ print "subject_coverage", $$0; } \
	}') > $@





# evalue distribution of best hits
nr.distribplot.eval.pdf: nr.tab
	cut -f 14 $< \
	| distrib_plot --type histogram --breaks 20 --remove-na --output $@ 1

# bit score distribution of best hits
nr.distribplot.bitscore.pdf: nr.tab
	cut -f 13 $< \
	| distrib_plot --type histogram --breaks 20 --remove-na --output $@ 1





# DOWNLOAD SWISS-PROT AS MASK FOR NR
# swissprot.flag:
# 	_comment () { echo -n ""; }; \
# 	mkdir $(basename $@) && \
# 	cd $(basename $@) && \
# 	date | bawk '!/^[$$,\#+]/ { \
# 	{ print "database_downloaded:",$$0; } \
# 	}' > DB.info && \
# 	_comment "This database is a mask for nr" && \
# 	wget -q ftp://ftp.ncbi.nih.gov/blast/db/$(basename $@).tar.gz && \
# 	wget -q ftp://ftp.ncbi.nih.gov/blast/db/$(basename $@).tar.gz.md5 && \
# 	test_blast_db $(basename $@).tar.gz $(basename $@).tar.gz.md5 && \
# 	tar xvfz $(basename $@).tar.gz && \
# 	rm $(basename $@).tar.gz  && \
# 	cd .. && \
# 	touch $@


# BLAST ON SWISS-PROT AS MASK FOR NR
# swissprot_result.xml: nr_no-blast-hit_lst.fasta swissprot.flag $(BLAST_DB_PATH)
# 	cd $(basename $^2) && \
# 	for NR in $^3/nr.* ; do \
# 	ln -sf $$NR ; \
# 	done && \
# 	cd .. && \
# 	blastx -evalue 0.001 -num_threads $(CPUS) -outfmt 5 -max_target_seqs 20 -query $< -db ./$(basename $^2)/$(basename $^2) -out $@



# DOWNLOAD SWISS-PROT AS FASTA
swissprot.flag:
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $@) && \
	cd $(basename $@) && \
	date | bawk '!/^[$$,\#+]/ { \
	{ print "database_downloaded:",$$0; } \
	}' > DB.info && \
	_comment "This database is in fasta format" && \
	wget -q -O $(basename $@).gz ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz && \
	gunzip $(basename $@).gz && \
	makeblastdb -in $(basename $@) -dbtype prot -title $(basename $@) && \
	rm $(basename $@) && \
	cd .. && \
	touch $@


# BLAST ON SWISS-PROT AS BLAST-DB FROM FASTA
swissprot.xml.gz: query.fasta swissprot.flag
	blastx -evalue 0.001 -num_threads $(CPUS) -outfmt 5 -max_target_seqs 20 -query $< -db ./$(basename $^2)/$(basename $^2) -out $(basename $@); \
	if gzip -cv $(basename $@) > $@; \
	then \
	rm $(basename $@); \
	fi


swissprot.tab: swissprot.xml.gz
	zcat -dc $< | blast_fields -f -r application,database_length,database,database_sequences,expect,query,query_length -a accession,hit_def,hit_id,length -p align_length,bits,expect,frame,gaps,identities,num_alignments,positives,query,query_end,query_start,sbjct,sbjct_end,sbjct_start,score,strand -e $(EVAL) > $@


# average number of hits per transcript
# average alignment lengths of best hit
# average subject coverage fraction of best hits
# average query coverage fraction of best hits
swissprot.hit.aln.info: swissprot.all.tab swissprot.tab
	paste -d "\n" \
	<(cut -f 10 $< | bsort | uniq -c | bawk '!/^[$$,\#+]/ { \
	split($$0,a," ");\
	print a[1]; \
	}' | stat_base --mean --median --stdev --variance --min --max --total 1 \
	| bawk 'BEGIN { print "#mean\tmedian\tstdev\tvariance\tmin\tmax\ttotal"; } \
	!/^[$$,\#+]/ { \
	{ print "subject_per_sequence", $$0; } \
	}') \
	<(cut -f 12 $^2 \
	| stat_base --mean --median --stdev --variance --min --max --total 1 \
	| bawk '!/^[$$,\#+]/ { \
	{ print "alignment_lengths", $$0; } \
	}') \
	<(cut -f 7,12 $^2 |  bawk '!/^[$$,\#+]/ { \
	{ printf "%.4f\n", (($$2*3)/$$1*100); } \
	}' \
	| stat_base --mean --median --stdev --variance --min --max \
	| bawk '!/^[$$,\#+]/ { \
	{ print "query_coverage", $$0; } \
	}') \
	<(cut -f 11,12 $^2 |  bawk '!/^[$$,\#+]/ { \
	{ printf "%.4f\n", ($$2/$$1*100); } \
	}' \
	| stat_base --mean --median --stdev --variance --min --max \
	| bawk '!/^[$$,\#+]/ { \
	{ print "subject_coverage", $$0; } \
	}') > $@



swissprot-nr.hit.info: nr.hit.lst swissprot.hit.lst
	paste <(comm -2 -3 --check-order <(bsort $<) <(bsort $^2) | wc -l) \
	<(comm -1 -3 --check-order <(bsort $<) <(bsort $^2) | wc -l) \
	<(comm -1 -2 --check-order <(bsort $<) <(bsort $^2) | wc -l) | \
	bawk -v file_1=$< -v file_2=$^2 \
	'BEGIN { printf "#unique_%s\tunique_%s\tcommon\n", file_1, file_2; } \
	!/^[$$,\#+]/ { \
	{ print $$0; } \
	}' > $@


swissprot-nr.hit.lst: nr.hit.lst swissprot.hit.lst
	comm -1 -3 --check-order <(bsort $<) <(bsort $^2) > $@



# BLAST ON NT BLAST-DB
# nt.tab: nt.xml
# 	blast_fields -f -r application,database_length,database,database_sequences,expect,query,query_length -a accession,hit_def,hit_id,length -p align_length,bits,expect,frame,gaps,identities,num_alignments,positives,query,query_end,query_start,sbjct,sbjct_end,sbjct_start,score,strand $< > $@

# nt.all.tab: nt.xml
# 	blast_fields -r application,database_length,database,database_sequences,expect,query,query_length -a accession,hit_def,hit_id,length -p align_length,bits,expect,frame,gaps,identities,num_alignments,positives,query,query_end,query_start,sbjct,sbjct_end,sbjct_start,score,strand $< > $@



nt.hit.aln.info: nt.all.tab nt.tab
	paste -d "\n" \
	<(cut -f 10 $< | bsort | uniq -c | bawk '!/^[$$,\#+]/ { \
	split($$0,a," ");\
	print a[1]; \
	}' | stat_base --mean --median --stdev --variance --min --max --total 1 \
	| bawk 'BEGIN { print "#mean\tmedian\tstdev\tvariance\tmin\tmax\ttotal"; } \
	!/^[$$,\#+]/ { \
	{ print "subject_per_sequence", $$0; } \
	}') \
	<(cut -f 12 $^2 \
	| stat_base --mean --median --stdev --variance --min --max --total 1 \
	| bawk '!/^[$$,\#+]/ { \
	{ print "alignment_lengths", $$0; } \
	}') \
	<(cut -f 7,12 $^2 | bawk '!/^[$$,\#+]/ { \
	{ printf "%.4f\n", ($$2/$$1*100); } \
	}' \
	| stat_base --mean --median --stdev --variance --min --max \
	| bawk '!/^[$$,\#+]/ { \
	{ print "query_coverage", $$0; } \
	}') \
	<(cut -f 11,12 $^2 |  bawk '!/^[$$,\#+]/ { \
	{ printf "%.4f\n", ($$2/$$1*100); } \
	}' \
	| stat_base --mean --median --stdev --variance --min --max \
	| bawk '!/^[$$,\#+]/ { \
	{ print "subject_coverage", $$0; } \
	}') > $@


nt-nr.hit.info: nr.hit.lst nt.hit.lst
	paste <(comm -2 -3 --check-order <(bsort $<) <(bsort $^2) | wc -l) \
	<(comm -1 -3 --check-order <(bsort $<) <(bsort $^2) | wc -l) \
	<(comm -1 -2 --check-order <(bsort $<) <(bsort $^2) | wc -l) \
	| bawk -v file_1=$< -v file_2=$^2 \
	'BEGIN { printf "#unique_%s\tunique_%s\tcommon\n", file_1, file_2; } \
	!/^[$$,\#+]/ { \
	{ print $$0; } \
	}' > $@


nt-nr.hit.lst: nr.hit.lst nt.hit.lst
	comm -1 -3 --check-order <(bsort $<) <(bsort $^2) > $@


nt+swissprot-nr.hit.info: nr.hit.lst swissprot-nr.hit.lst nt.hit.lst
	paste <(comm -2 -3 --check-order <(cat $< $^2 | bsort) <(bsort $^3) | wc -l) \
	<(comm -1 -3 --check-order <(cat $< $^2 | bsort) <(bsort $^3) | wc -l) \
	<(comm -1 -2 --check-order <(cat $< $^2 | bsort) <(bsort $^3) | wc -l) \
	| bawk -v file_1=$<+$^2 -v file_2=$^3 \
	'BEGIN { printf "#unique_%s\tunique_%s\tcommon\n", file_1, file_2; } \
	!/^[$$,\#+]/ { \
	{ print $$0; } \
	}' > $@

nt+swissprot-nr.hit.lst: nr.hit.lst swissprot-nr.hit.lst nt.hit.lst
	cat <(comm -2 -3 --check-order <(cat $< $^2 | bsort) <(bsort $^3)) \
	<(comm -1 -3 --check-order <(cat $< $^2 | bsort) <(bsort $^3)) \
	<(comm -1 -2 --check-order <(cat $< $^2 | bsort) <(bsort $^3)) > $@

nt+swissprot-nr.no-hit.lst: nt+swissprot-nr.hit.lst query.fasta
	comm -2 -3 --check-order <(bsort <(fasta2tab < $^2 | cut -f 1)) \
	<(bsort $<) > $@



.PHONY: test
test:
	@echo $(454_INPUT)


# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += query.fasta \
	 nr.distribplot.bitscore.pdf \
	 nr.distribplot.eval.pdf \
	 nr.hit.aln.info \
	 nr.hit.contig.lst \
	 nr.hit.fasta \
	 nr.hit.lst \
	 nr.hit.singletons.lst \
	 nr.no-hit.contig.lst \
	 nr.no-hit.fasta \
	 nr.no-hit.lst \
	 nr.no-hit.singletons.lst \
	 nr.subject.lst \
	 nr.tab \
	 nr.xml.gz \
	 nt.all.tab \
	 nt.hit.aln.info \
	 nt.hit.lst \
	 nt.no-hit.lst \
	 nt-nr.hit.info \
	 nt-nr.hit.lst \
	 nt.subject.lst \
	 nt+swissprot-nr.hit.info \
	 nt+swissprot-nr.hit.lst \
	 nt+swissprot-nr.no-hit.fasta \
	 nt+swissprot-nr.no-hit.lst \
	 nt.tab \
	 nt.xml.gz \
	 swissprot.all.tab \
	 swissprot.flag \
	 swissprot.hit.aln.info \
	 swissprot.hit.lst \
	 swissprot.no-hit.lst \
	 swissprot-nr.hit.info \
	 swissprot-nr.hit.lst \
	 swissprot.subject.lst \
	 swissprot.tab \
	 swissprot.xml.gz \
	 nr.boxplot.GC.pdf \
	 nr.boxplot.len.pdf \
	 nr.boxplot.qual.pdf

# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += swissprot \
	 $(TMP_DIR) \
	 query.fasta \
	 nr.distribplot.bitscore.pdf \
	 nr.distribplot.eval.pdf \
	 nr.hit.aln.info \
	 nr.hit.contig.lst \
	 nr.hit.fasta \
	 nr.hit.lst \
	 nr.hit.singletons.lst \
	 nr.no-hit.contig.lst \
	 nr.no-hit.fasta \
	 nr.no-hit.lst \
	 nr.no-hit.singletons.lst \
	 nr.subject.lst \
	 nr.tab \
	 nr.xml.gz \
	 nt.all.tab \
	 nt.hit.aln.info \
	 nt.hit.lst \
	 nt.no-hit.lst \
	 nt-nr.hit.info \
	 nt-nr.hit.lst \
	 nt.subject.lst \
	 nt+swissprot-nr.hit.info \
	 nt+swissprot-nr.hit.lst \
	 nt+swissprot-nr.no-hit.fasta \
	 nt+swissprot-nr.no-hit.lst \
	 nt.tab \
	 nt.xml.gz \
	 swissprot.all.tab \
	 swissprot.flag \
	 swissprot.hit.aln.info \
	 swissprot.hit.lst \
	 swissprot.no-hit.lst \
	 swissprot-nr.hit.info \
	 swissprot-nr.hit.lst \
	 swissprot.subject.lst \
	 swissprot.tab \
	 swissprot.xml.gz \
	 nr.boxplot.GC.pdf \
	 nr.boxplot.len.pdf \
	 nr.boxplot.qual.pdf


######################################################################
### phase_1.mk ends here
