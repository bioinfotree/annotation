### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:



TEST ?=

B2GO_GAF ?=
B2GO_KEGG ?=

# links
b2go.GAF: $(B2GO_GAF)
	ln -sf $< $@

b2go.kegg: $(B2GO_KEGG)
	ln -sf $< $@



GAF.info: b2go.GAF
	paste -d "\n" \
	<(TIPE="P" && \
	bawk -v type=$$TIPE '!/^[$$,\#+]/ { \
	if ($$9 == type) \
	{ \
	print $$0; \
	}; \
	}' $< | cut -f 5 | bsort | uniq -c | wc -l | \
	bawk -v type=$$TIPE '!/^[$$,\#+]/ { \
	printf "unique_%s_terms\t%i", type, $$0; \
	}') \
	<(TIPE="C" && \
	bawk -v type=$$TIPE '!/^[$$,\#+]/ { \
	if ($$9 == type) \
	{ \
	print $$0; \
	}; \
	}' $< | cut -f 5 | bsort | uniq -c | wc -l | \
	bawk -v type=$$TIPE '!/^[$$,\#+]/ { \
	printf "unique_%s_terms\t%i", type, $$0; \
	}') \
	<(TIPE="F" && \
	bawk -v type=$$TIPE '!/^[$$,\#+]/ { \
	if ($$9 == type) \
	{ \
	print $$0; \
	}; \
	}' $< | cut -f 5 | bsort | uniq -c | wc -l | \
	bawk -v type=$$TIPE '!/^[$$,\#+]/ { \
	printf "unique_%s_terms\t%i", type, $$0; \
	}') > $@




KEGG.info: b2go.kegg
	paste -d "\n" \
	<(cut -f 1,7 $< | bsort --key=2,2 | uniq -c | bsort --key=1,1 | wc -l | \
	bawk '!/^[$$,\#+]/ { \
	{ print "unique_KEGG_pathways", $$0; } \
	}') \
	<(cut -f 5 $< | bawk '/^[0-9]/' | stat_base --total 1 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "total_sequences", $$0; } \
	}') \
	<(bawk '!/^[$$,\#+]/ { \
	{ if ( $$4 ~ /^ec/ ) print $$4; } \
	}' $< | bsort | uniq -c | wc -l | \
	bawk '!/^[$$,\#+]/ { \
	{ print "total_EC_codes", $$0; } \
	}') > $@


sorted_pathway_crowding: b2go.kegg
	cut -f 1,7 $< | bsort --key=2,2 | tr -s " " "_" | uniq -c \
	| bsort --key=1,1 | tr -s " " \\\t | tr -s "_" " " > $@


.PHONY: test
test:
	@echo $(454_INPUT)


# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += GAF.info \
	 KEGG.info \
	 sorted_pathway_crowding

# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += b2go.kegg \
	 b2go.GAF \
	 GAF.info \
	 KEGG.info \
	 sorted_pathway_crowding


# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:


######################################################################
### phase_1.mk ends here
