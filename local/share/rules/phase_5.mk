### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:



TEST ?=
PRJ ?=
INPUT_FASTA ?=
DB-PATH_NR ?=
DB-NAME_NR ?=
CPUS ?=


extern ../phase_1/swissprot/swissprot as SWISSPROT_DB
#extern /home/michele/Research/Projects/Transcriptomic_analysis/Data/cDNAstorione/all_genome_databases/Acipenser_NCBI_proteins/Acipenser_NCBI_proteins.fasta.strained as SWISSPROT_DB
extern ../phase_1/swissprot.xml.gz as SWISSPROT_XML

#extern ../phase_1/nt+swissprot-nr.no-hit.fasta as NT_SWISSPROT-NR_NO-HIT_FASTA

extern ../phase_1/nr.no-hit.fasta as NR_NO-HIT_FASTA


# links
query.fasta: $(INPUT_FASTA)
	ln -sf $< $@

# nt+swissprot-nr.no-hit.fasta: $(NT_SWISSPROT-NR_NO-HIT_FASTA)
# 	ln -sf $< $@

nr.no-hit.fasta: $(NR_NO-HIT_FASTA)
	ln -sf $< $@


# USE PROT4EST + ESTSCAN

p4e_access.txt:
	echo -e "\
	#Access file for prot4EST suite of programs.\n\
	#This file typically needs to be set once for a run of all the programs.\n\
	#Please enter the FULL file path for each variable\n\
	TAXDIR='./taxonomy/';\n\
	P4ELIB='$$HOME/bioinfotree/prj/annotation/local/src/p4e3.1b/lib/';\n\
	ESTSCANDIR='$$HOME/bioinfotree/prj/annotation/local/src/estscan-3.0.3/';\n\
	ESTSCANLIB='$$HOME/bioinfotree/prj/annotation/local/src/estscan-3.0.3/';\n\
	BLASTMAT='BLOSUM62';\n\
	BLASTDB='$(SWISSPROT_DB)';" > $@


ALC_sim.config:
	echo -e "\
	#Configuration File for simTrans.pl\n\
	\n\
	NUCLEOTIDE_FILE='./query.fasta';\n\
	\n\
	SPECIES='Acipenser naccarii';\n\
	TAXID='42330';\n\
	\n\
	BLAST_REPORT='./ALCvUniref.blx';\n\
	\n\
	PROTEIN_FILE='./wormpep172_pro.fsa';\n\
	\n\
	#GENETIC_CODE='6';\n\
	CLEAN='1';" > $@


ALC_smat.config:
	echo -e "\
	#Configuration File for constructSMAT.pl\n\
	\n\
	SPECIES='Acipenser naccarii';\n\
	#TAXID='42330';\n\
	\n\
	FSA_FILE='./A.naccarii_sim.fsa';\n\
	EMBL_FILE='';\n\
	EMBL_SEARCH='0';\n\
	CLEAN='1';" > $@

#qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore
# ALCvUniref.blx: $(SWISSPROT_XML)
# 	blast_fields -r query -a hit_def -p identities,align_length,num_alignments,gaps,query_start,query_end,sbjct_start,sbjct_end,expect,bits $< > $@

#-r application,database_length,database,database_sequences,expect,,query_length -a accession,,,length -p ,,,frame,gaps,,num_alignments,positives,query,,query_start,sbjct,,,score,strand $1 > $2


ALCvUniref.blx: query.fasta $(SWISSPROT_DB)
	blastall -p blastx -e 0.001 -a $(CPUS) -m 8 -i $< -d $^2 -o $@



taxonomy.flag:
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $@) && \
	cd $(basename $@) && \
	date | bawk '!/^[$$,\#+]/ { \
	{ print "database_downloaded:",$$0; } \
	}' > DB.info && \
	_comment "" && \
	wget -q -O $(basename $@)dmp.zip ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdmp.zip && \
	unzip $(basename $@)dmp.zip && \
	rm $(basename $@)dmp.zip && \
	cd .. && \
	touch $@



.PHONY: simtrans, constructsmat

A.naccarii_sim.fsa: p4e_access.txt ALC_sim.config taxonomy.flag
	simTrans --access p4e_access.txt --config ALC_sim.config


constructsmat: p4e_access.txt ALC_smat.config A.naccarii_sim.fsa
	constructSMAT --access p4e_access.txt --config ALC_smat.config

#########################################################################

# USE ORFPREDICTOR

# orf_predictor.fasta: nt+swissprot-nr.no-hit.fasta
# 	BLAST="fake_blast.blx" && \
# 	touch $$BLAST && \
# 	OrfPredictor_web3 $< $$BLAST 0 both fake_email 0.001 $@ fake_url && \
# 	rm $$BLAST


orf_predictor.fasta: nr.no-hit.fasta
	BLAST="fake_blast.blx" && \
	touch $$BLAST && \
	OrfPredictor_web3 $< $$BLAST 0 both fake_email 0.001 $@ fake_url && \
	rm $$BLAST



orf_predictor.info: orf_predictor.fasta
	paste -d "\n" \
	<(fastalen $< | bawk '!/^[$$,\#+]/ { \
	print $$4-$$3; \
	}' | stat_base --mean --median --stdev --variance --min --max --total 1 \
	| bawk 'BEGIN { print "#mean\tmedian\tstdev\tvariance\tmin\tmax\ttotal"; } \
	!/^[$$,\#+]/ { \
	{ print "ORF_lenght", $$0; } \
	}') \
	<(fastalen $< | stat_base --mean --median --stdev --variance --min --max --total 5 \
	| bawk '!/^[$$,\#+]/ { \
	{ print "protein_lenght", $$0; } \
	}') > $@



.PHONY: test
test:
	@echo $(454_INPUT)


# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += orf_predictor.fasta \
	 orf_predictor.info

# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += $(TMP_DIR) \
	 swissprot \
	 orf_predictor.fasta \
	 orf_predictor.info


######################################################################
### phase_1.mk ends here
