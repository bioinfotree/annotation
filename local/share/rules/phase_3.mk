### phase_3.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

context prj/454_preprocessing

TEST ?=
PRJ ?=
INPUT_FASTA ?=
INPUT_QUAL ?=
CPUS ?=
SPECIE ?=

DB-PATH_ACIPENSER_PROT ?=
DB-NAME_ACIPENSER_PROT ?=
EVAL ?=

# remove elephant-shark from specie list, no proteins to be analyzed
THIS_SPECIE = $(call set_remove,elephant-shark,$(call set_create, $(SPECIE)))

# links
query.fasta: $(INPUT_FASTA)
	ln -sf $< $@



# ACIPENSER PROT
acipenser_PROT.flag: $(DB-PATH_ACIPENSER_PROT)
	$(call link_DB, $</$(DB-NAME_ACIPENSER_PROT)*, $@)

acipenser_PROT.xml: query.fasta acipenser_PROT.flag
	$(call search_DB, $<, ./$(basename $^2)/$(basename $^2), $@)







######################################################################################
SPECIE_XML_GZ = $(addsuffix _PROT.xml.gz, $(THIS_SPECIE))
%_PROT.xml.gz: query.fasta %_PROT.flag
	!threads
	$(call search_DB, $<, ./$(basename $^2)/$(basename $^2), $@)

SPECIE_TAB = $(addsuffix _PROT.tab, $(THIS_SPECIE))
%_PROT.tab: %_PROT.xml.gz
	$(call parse_result, $<, $@)

SPECIE_NO_HL = $(addsuffix _PROT.no-hit.lst, $(THIS_SPECIE))
# list of sequences whithout hit
%_PROT.no-hit.lst: %_PROT.tab query.fasta
	comm -2 -3 --check-order <(bsort <(fasta2tab < $^2 | cut -f 1)) \
	<(bsort <(cut -f 6 < $<)) > $@

SPECIE_HL = $(addsuffix _PROT.hit.lst, $(THIS_SPECIE))
%_PROT.hit.lst: %_PROT.tab
	cut -f 6 < $< > $@

SPECIE_SBJ = $(addsuffix _PROT.subject.lst, $(THIS_SPECIE))
%_PROT.subject.lst: %_PROT.tab
	cut -f 9 $< | bsort | uniq -c > $@

SPECIE_GENE = $(addsuffix _PROT.gene.lst, $(THIS_SPECIE))
%_PROT.gene.lst: %_PROT.tab
	cut -f 9 $< | cut -d " " -f 4 | bsort | uniq -c > $@

SPECIE_TOT_GENE = $(addsuffix _PROT.tot_gene.lst, $(THIS_SPECIE))
%_PROT.tot_gene.lst: %_PROT.flag
	fasta2tab < $(basename $<)/$(basename $<) | cut -d " " -f 4 | bsort | uniq -c > $@





define link_DB
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $2) && \
	cd $(basename $2) && \
	_comment "cDNA sequence downloaded from genome project" && \
	for FILE in $1 ; do \
	ln -sf $$FILE ; \
	done && \
	cd .. && \
	touch $2
endef



define download_DB
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $3) && \
	cd $(basename $3) && \
	date | bawk '!/^[$$,\#+]/ { \
	{ print "database_downloaded:",$$0; } \
	}' > DB.info && \
	_comment "protein sequence downloaded from ensembl release 66" && \
	wget -q -O $(basename $3).gz $1 && \
	wget -q  $2 && \
	gunzip $(basename $3).gz && \
	_comment "some sequences from ENSEMBL can contain *. Remove them" && \
	sed -i 's/*//g' $(basename $3) && \
	makeblastdb -in $(basename $3) -dbtype prot -title $(basename $3) && \
	cd .. && \
	touch $3
endef


define search_DB
	blastx -evalue 0.001 -num_threads $$THREADNUM -outfmt 5 -max_target_seqs 20 -query $1 -db $2 -out $(basename $@); \
	if gzip -cv $(basename $@) > $@; \
	then \
	rm $(basename $@); \
	fi
endef


define parse_result
	zcat -dc $1 | blast_fields -f -r application,database_length,database,database_sequences,expect,query,query_length -a accession,hit_def,hit_id,length -p align_length,bits,expect,frame,gaps,identities,num_alignments,positives,query,query_end,query_start,sbjct,sbjct_end,sbjct_start,score,strand -e $(EVAL) > $2
endef



#######################################################################################

SPECIE_DB = $(addsuffix _PROT, $(THIS_SPECIE))
SPECIE_FLAG = $(addsuffix _PROT.flag, $(THIS_SPECIE))





# LAMPREY proteins
LAMPREY_PROT = ftp://ftp.ensembl.org/pub/release-66/fasta/petromyzon_marinus/pep/Petromyzon_marinus.Pmarinus_7.0.66.pep.all.fa.gz
README = ftp://ftp.ensembl.org/pub/release-66/fasta/petromyzon_marinus/pep/README

lamprey_PROT.flag:
	$(call download_DB, $(LAMPREY_PROT), $(README), $@)




# DANIO proteins
DANIO_PROT = ftp://ftp.ensembl.org/pub/release-66/fasta/danio_rerio/pep/Danio_rerio.Zv9.66.pep.all.fa.gz

danio_PROT.flag:
	$(call download_DB, $(DANIO_PROT), $(README), $@)



# FUGU proteins
FUGU_PROT = ftp://ftp.ensembl.org/pub/release-66/fasta/takifugu_rubripes/pep/Takifugu_rubripes.FUGU4.66.pep.all.fa.gz

fugu_PROT.flag:
	$(call download_DB, $(FUGU_PROT), $(README), $@)


# TETRAODON proteins
TETRAODON_PROT = ftp://ftp.ensembl.org/pub/release-66/fasta/tetraodon_nigroviridis/pep/Tetraodon_nigroviridis.TETRAODON8.66.pep.all.fa.gz

tetraodon_PROT.flag:
	$(call download_DB, $(TETRAODON_PROT), $(README), $@)


# STICKLEBACK proteins
STICKLEBACK_PROT = ftp://ftp.ensembl.org/pub/release-66/fasta/gasterosteus_aculeatus/pep/Gasterosteus_aculeatus.BROADS1.66.pep.all.fa.gz

stickleback_PROT.flag:
	$(call download_DB, $(STICKLEBACK_PROT), $(README), $@)



# MEDAKA proteins
MEDAKA_PROT = ftp://ftp.ensembl.org/pub/release-66/fasta/oryzias_latipes/pep/Oryzias_latipes.MEDAKA1.66.pep.all.fa.gz

medaka_PROT.flag:
	$(call download_DB, $(MEDAKA_PROT), $(README), $@)



# COELACANTH proteins
COELACANTH_PROT = ftp://ftp.ensembl.org/pub/release-66/fasta/latimeria_chalumnae/pep/Latimeria_chalumnae.LatCha1.66.pep.all.fa.gz

coelacanth_PROT.flag:
	$(call download_DB, $(COELACANTH_PROT), $(README), $@)


# COD proteins
COD_PROT = ftp://ftp.ensembl.org/pub/release-66/fasta/gadus_morhua/pep/Gadus_morhua.gadMor1.66.pep.all.fa.gz

cod_PROT.flag:
	$(call download_DB, $(COD_PROT), $(README), $@)


# HSAPIENS proteins
HSAPIENS_PROT = ftp://ftp.ensembl.org/pub/release-66/fasta/homo_sapiens/pep/Homo_sapiens.GRCh37.66.pep.all.fa.gz

hsapiens_PROT.flag:
	$(call download_DB, $(HSAPIENS_PROT), $(README), $@)







.PHONY: test
test:
	@echo $(THIS_SPECIE)


# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += query.fasta \
	 $(SPECIE_XML_GZ) \
	 $(SPECIE_HL) \
	 $(SPECIE_NO_HL) \
	 $(SPECIE_SBJ) \
	 $(SPECIE_GENE) \
	 $(SPECIE_TOT_GENE)

# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += query.fasta \
	 $(SPECIE_XML_GZ) \
	 $(SPECIE_TAB) \
	 $(SPECIE_NO_HL) \
	 $(SPECIE_HL) \
	 $(SPECIE_GENE) \
	 $(SPECIE_SBJ) \
	 $(SPECIE_TOT_GENE)
	 #$(SPECIE_DB)
	 #$(SPECIE_FLAG) 


######################################################################
### phase_3.mk ends here
