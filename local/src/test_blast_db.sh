#!/bin/bash

#Compares the checksums of a blast db archive and the corresponding md5 results.
#This script is released into the public domain by it's author Michele Vidotto.

usage()
{
cat << EOF
usage: $0 [OPTIONS] DB_ARCHIVE MD5_FILE

Compares the checksums of a blast db archive and the corresponding md5 file.

OPTIONS:
   -h      Show this message
EOF
}

archive=""
md5_file=""

while getopts "h" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
     esac
done


if [ -n "$1" ]; then
archive="$1"
else
archive=""
fi

if [ -n "$2" ]; then
md5_file="$2"
else
md5_file=""
fi


if [[ -z $archive ]] || [[ -z $md5_file ]]
then
     usage
     exit 1
fi



correct=`more $md5_file | grep --only-matching -m 1 '^[0-9a-f]*'`;
md5=`md5sum $archive | grep --only-matching -m 1 '^[0-9a-f]*'`;

#echo $correct;
#echo $md5;
  
        if [ "$md5" = "$correct" ];
        then
                echo "md5('$md5') is equal to correct('$correct')"
		exit 0
        fi
	if [ "$md5" != "$correct" ];
        then
                echo "md5('$md5') is not equal to correct('$correct')"
		exit 1
        fi

