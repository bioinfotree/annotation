#!/usr/bin/env python
#
# Copyright 2012 Alessandro Coppe <alexcoppe@gmail.com>
# Copyright 2012 Michele Vidotto <michele.vidotto@gmail.com>
#
# This file is licensed under the
# terms of the GNU Affero General Public License version 3.

from __future__ import division
from optparse import OptionParser
from Bio.Blast import NCBIXML
from vfork.util import exit, format_usage
from vfork.util import ignore_broken_pipe
from sys import stdin
from math import fabs


def flatten(lst):
    for elem in lst:
        if getattr(elem,'__iter__',False):
            for i in flatten(elem):
                yield i
        else:
            yield elem


def buildAttributesString(object, attributes, sep="\t"):
    return [str(getattr(object, attribute)) for attribute in attributes]

def checkAttributes(recordObject, recordAttrs, alignmentAttrs, hspAttrs):
    attributes = set(dir(recordObject))
    difference = set(recordAttrs).difference(attributes)
    if difference:
        return difference,attributes
    attributes = set(dir(recordObject.alignments[0]))
    difference = set(alignmentAttrs).difference(attributes)
    if difference:
        return difference,attributes
    attributes = set(dir(recordObject.alignments[0].hsps[0]))
    difference = set(hspAttrs).difference(attributes)
    if difference:
        return difference,attributes
    return [],attributes


# filters implementations
def hspIsFilterd(hsp, alignment, record, options):
    #Filter out hsp with evalue > of threshold
    if options.evalue is not None:
        if hsp.expect > options.evalue:
            return True
    #Filter out hsp with percentage of subject covered by alignemnt < of threshold
    if options.subcov is not None:
        hspCoverage =  fabs( (hsp.sbjct_end + 1) - hsp.sbjct_start)
        coveragePercent = hspCoverage / alignment.length
        if coveragePercent < options.subcov:
            return True
    #Filter out hsp with percentage of query covered by alignemnt < of threshold
    if options.querycov is not None:
        hspCoverage = fabs( (hsp.query_end + 1) - hsp.query_start)
        coveragePercent = hspCoverage / record.query_length
        if coveragePercent < options.querycov:
            return True
    #Filter out hsp with identities < of threshold
    if options.id is not None:
        if hsp.identities < options.id:
            return True
    #Filter out hsp with positives < of threshold
    if options.pos is not None:
        if hsp.positives < options.pos:
            return True
    #Filter out hsp with raw score < of threshold
    if options.score is not None:
        if hsp.score < options.score:
            return True
    #Filter out hsp by bit score < of threshold
    if options.bscore is not None:
        if hsp.bits < options.bscore:
            return True
    #Filter out hsp with gaps > of threshold
    if options.gap is not None:
        if hsp.gaps > options.gap:
            return True

    return False
        


def getRecordFields(blastRecord, recordAttributes, alignmentAttributes, hspAttributes, options, first=False):
    attributesDifference,availableAttributes = checkAttributes(blastRecord, recordAttributes, alignmentAttributes, hspAttributes)
    if attributesDifference:
        availableAttributes =  ", ".join(availableAttributes)
        out = "Error: the following attributes could not be found %s\nValid attributes are: %s" % (" ".join(attributesDifference), availableAttributes)
        return -1,out
    recordList = buildAttributesString(blastRecord, recordAttributes)
    alignments = blastRecord.alignments
    entries = []
    for alignment in alignments:
        alignmentList = buildAttributesString(alignment, alignmentAttributes)
        for hsp in alignment.hsps:
            filtered = hspIsFilterd(hsp, alignment, blastRecord,  options)
            if not filtered:
                hspString = buildAttributesString(hsp, hspAttributes)
                entries.append((recordList, alignmentList, hspString))
                if first: return 0,entries
    return 0,entries


def collapseAttributesList(out, sep="\t"):
    l = list(flatten(out))
    return sep.join(l)

def prettyPrintAttributes(obj, attributes):
    print "Attributes available for", obj.__class__, ":"
    print ",".join(attributes)
    print



def showAttributes(record):
    recordAttributes = dir(record)
    recordAttributes = [element for element in recordAttributes if element[0:2] != "__"]
    prettyPrintAttributes(record, recordAttributes)

    alignmentAttributes = dir(record.alignments[0])
    alignmentAttributes = [element for element in alignmentAttributes if element[0:2] != "__"]
    prettyPrintAttributes(record.alignments[0], alignmentAttributes)

    hspAttributes = dir(record.alignments[0].hsps[0])
    hspAttributes = [element for element in hspAttributes if element[0:2] != "__"]
    prettyPrintAttributes(record.alignments[0].hsps[0], hspAttributes)

def main():
    parser = OptionParser(usage=format_usage('''
        Usage: %prog [OPTIONS] <XML_BLAST_RESULT >TAB

        Convert BLAST results file in XML format into TSV file with selected fields
        specified by comma separated format specifiers, for the attributes of three
        alignment objects:
        
        1) record
        2) alignment
        3) hsp

        Performs some filters on hsps, if indicated
    '''))

    parser.add_option("-f", "--first-only", help="Display only first match (a.k.a. best match)", action="store_true", dest="first")
    parser.add_option("-l", "--list-attributes", help="Lists all available attributes for blast record object", action="store_true", dest="list")
    parser.add_option("-r", "--record-attributes", help="Comma separated attributes for the record object", dest="recordAttributes")
    parser.add_option("-a", "--alignment-attributes", help="Comma separated attributes for the alignment object", dest="alignmentAttributes")
    parser.add_option("-p", "--hspAttributes-attributes", help="Comma separated attributes for the hsp object", dest="hspAttributes")
    
    # filters
    parser.add_option("-e", "--min-evalue", help="Filter out hsps with evalue > of threshold.", dest="evalue", type="float")
    parser.add_option("-s", "--min-sbj-cov", help="Filter out hsp with percentage of subject covered by alignemnt < of threshold. [0.0-1.0]", dest="subcov", type="float")
    parser.add_option("-q", "--min-query-cov", help="Filter out hsp with percentage of query covered by alignemnt < of threshold. [0.0-1.0]", dest="querycov", type="float")
    parser.add_option("-i", "--min-identity", help="Filter out hsp with identities < of threshold. [>0]", dest="id", type="int")
    parser.add_option("-m", "--min-positive", help="Filter out hsp with positives < of threshold [>0]", dest="pos", type="int")
    parser.add_option("-n", "--min-score", help="Filter out hsp with raw score < of threshold. [>0.0]", dest="score", type="float")
    parser.add_option("-b", "--min-bitscore", help="Filter out hsp with bit score < of threshold [>0.0]", dest="bscore", type="float")
    parser.add_option("-g", "--min-gap", help="Filter out hsp with gaps > of threshold. [>0]", dest="gap", type="int")

    

    (options, args) = parser.parse_args()
    
    # filter limits
    if (options.subcov is not None) and (options.subcov < 0 or options.subcov > 1):
        parser.error("subject coverage must be [0-1]")
    if (options.querycov is not None) and (options.querycov < 0 or options.querycov > 1):
        parser.error("query coverage must be [0-1]")
    if (options.id is not None) and options.id < 0:
        parser.error("min indentity must be >= 0")
    if (options.pos is not None) and options.pos < 0:
        parser.error("min positive must be >= 0")
    if (options.score is not None) and options.score < 0:
        parser.error("min score must be >= 0")
    if (options.bscore is not None) and options.bscore < 0:
        parser.error("min bitscore must be >= 0")
    if (options.gap is not None) and options.gap < 0:
        parser.error("min gap must be >= 0")

    
    alignmentAttributes = []
    recordAttributes = []
    hspAttributes = []
    if options.alignmentAttributes: alignmentAttributes = options.alignmentAttributes.split(",")
    if options.recordAttributes: recordAttributes = options.recordAttributes.split(",")
    if options.hspAttributes: hspAttributes = options.hspAttributes.split(",")


    try:
        fin = stdin
    except:
        exit("Could not open file %s" % (stdin))

    records =  NCBIXML.parse(fin)
    if options.list:
        record = records.next()
        alignments = len(record.alignments)
        # extract first record with an alignment
        while alignments == 0:
            #print "------"
            ##pass
            #continue
            alignments = len(records.next().alignments)
        showAttributes(records.next())
        return 0

    for record in records:
        if record.alignments:
            if options.first:
                status,out = getRecordFields(record, recordAttributes, alignmentAttributes, hspAttributes, options, True)
            else:
                status,out = getRecordFields(record, recordAttributes, alignmentAttributes, hspAttributes, options, False)
            if status == -1:
                exit(out)
            for row in out:
                print collapseAttributesList(row)

if __name__ == "__main__":
    ignore_broken_pipe(main)
