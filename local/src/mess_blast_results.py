#!/usr/bin/env python

# mess_blast_results.py --- 
# 
# Filename: mess_blast_results.py
# Description: 
# Author: Alessandro Coppe
# Maintainer: 
# Created: Tue Feb 21 12:19:21 2012 (+0100)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:


import xml.sax
from xml.sax.saxutils import XMLGenerator
import sys
from optparse import OptionParser

# head = """
# <?xml version="1.0"?>
# <!DOCTYPE BlastOutput PUBLIC "-//NCBI//NCBI BlastOutput/EN" "http://www.ncbi.nlm.nih.gov/dtd/NCBI_BlastOutput.dtd">
# <BlastOutput>
#   <BlastOutput_program>blastx</BlastOutput_program>
#   <BlastOutput_version>blastx 2.2.18 [Mar-02-2008]</BlastOutput_version>
#   <BlastOutput_reference>~Reference: Altschul, Stephen F., Thomas L. Madden, Alejandro A. Schaffer, ~Jinghui Zhang, Zheng Zhang, Webb Miller, and David J. Lipman (1997), ~&quot;Gapped BLAST and PSI-BLAST: a new generation of protein database search~programs&quot;,  Nucleic Acids Res. 25:3389-3402.</BlastOutput_reference>
#   <BlastOutput_db>/home/ale/Code/Projects/454/databases/blastdbs/nr</BlastOutput_db>
#   <BlastOutput_query-ID>lcl|1_0</BlastOutput_query-ID>
#   <BlastOutput_query-def>eu2_c2</BlastOutput_query-def>
#   <BlastOutput_query-len>1424</BlastOutput_query-len>
#   <BlastOutput_param>
#     <Parameters>
#       <Parameters_matrix>BLOSUM62</Parameters_matrix>
#       <Parameters_expect>0.001</Parameters_expect>
#       <Parameters_gap-open>11</Parameters_gap-open>
#       <Parameters_gap-extend>1</Parameters_gap-extend>
#       <Parameters_filter>F</Parameters_filter>
#     </Parameters>
#   </BlastOutput_param>
#   <BlastOutput_iterations>
# """

tail = """
  </BlastOutput_iterations>
</BlastOutput>
"""



class BlastIterationHandler(xml.sax.handler.ContentHandler):
    def __init__(self):
        self.inIteration = False
        self.shit = ""
        self.logger = XMLGenerator(sys.stdout, "utf-8")
        self.header = str()
        self.tail = str()
    def setHead(self, head):
        self.head = head
    def setTail(self, tail):
        self.tail = tail
    def startElement(self, name, attrs):
        if name == "Iteration":
            self.inIteration = True
            sys.stdout.write(self.head)
        if self.inIteration:
            self.logger.startElement(name, {})
    def endElement(self, name):
        if self.inIteration:
            self.logger.endElement(name)
        if name == "Iteration":
            self.inIteration = False
            sys.stdout.write(self.tail)
    def characters(self, data):
        if self.inIteration:
            sys.stdout.write(data)
            pass


def grep_header(f):
    head = []
    begin = True
    line = f.readline()
    if line.strip() == """<?xml version="1.0"?>""":
        head.append(line)
        while begin:
            line = f.readline()
            head.append(line)
            if line.strip() == """<Iteration>""":
                return str()
            if line.strip() == """<BlastOutput_iterations>""":
                begin = False
    return str().join(head)


def main():
    usage = "usage: %prog [options] blastResultsFile"
    parser = OptionParser(usage=usage)
    (options, args) = parser.parse_args()
    if len(args) < 1:
        parser.error("Incorrect number of arguments")
    try:
        f = open(args[0])
    except:
        sys.exit("Could not open file: %s" % args[0])
    head = grep_header(f)
    if head == str():
        sys.exit("ERROR header not found!")
    h = BlastIterationHandler()
    h.setHead(head)
    h.setTail(tail)
    parser = xml.sax.make_parser()
    parser.setContentHandler(h)
    f = open(args[0])
    parser.parse(f)

if __name__ == "__main__":
    main()


# 
# mess_blast_results.py ends here
