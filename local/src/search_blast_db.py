# search_blast_db.py --- 
# 
# Filename: search_blast_db.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Mon Apr 18 18:20:50 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:

# Code:
from blast_tools.NCBI_BLAST import NCBIBLAST
import sys

# class test
def main():

    # in quary
    in_quary = sys.argv[1]
    
    # set out blast database 
    out_file = sys.argv[1]

    db = str(sys.argv[2])

    evalues_lst = [10]#, 1e-06, 1e-10, 1e-20, 1e-50]

    for evalue in evalues_lst:
        # set blast results file
        xml_file = u"all_genes%%_%s_CDNA4_only_644.xml" %(str(evalue)) 

        # create new object
        ncbiblast = NCBIBLAST()
    
        # search on previously created db
        ncbiblast.set_blast_bin(r"blastx")
        ncbiblast.set_db(db)
        ncbiblast.set_quary(in_quary)
        ncbiblast.set_out_file(xml_file)
        # set options for blast search
        common_options = [r"-evalue %s" % evalue, r"-num_threads 4", r"-outfmt 5", r"-max_target_seqs 20"]
        prog_specific_options = [r""]
        print ncbiblast.set_blast_settings(common_options, prog_specific_options)
        ncbiblast.search()

    return 0



if __name__ == "__main__":
    main()


# 
# search_blast_db.py ends here
